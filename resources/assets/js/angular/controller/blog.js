/**
 * Created on 26/08/2020
 */


app.controller('BlogController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm) {

        $scope.blogs = [];
        $scope.newblog = {};
        $scope.curblog = false;
        $scope.ss = false;
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.fileValidation = {};
        $scope.validationError = {};


        loadBlog();

        function loadBlog() {
            $http.get($rootScope.base_url + 'dashboard/blog/get').then(function (response) {
                console.log(response.data);
                if (response.data) {
                    $scope.blogs = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newBlog = function () {
            $scope.newblog = {};
            $scope.showform = true;
            $scope.curblog = false;
            $scope.validationError = [];
        };

        $scope.editBlog = function (item) {
            $scope.showform = true;
            $scope.curblog = item;
            $scope.newblog = angular.copy(item);
            $scope.validationError = [];
        };

        $scope.hideForm = function () {
            $scope.showform = false;
        };

        $scope.addBlog = function () {

            var fd = new FormData();

            angular.forEach($scope.newblog, function (item, key) {
                fd.append(key, item);
            });

            if ($scope.newblog['id']) {
                var url = $rootScope.base_url + 'dashboard/blog/edit/' + $scope.newblog.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        loadBlog();
                        $scope.newblog = {};
                        $scope.showform = false;
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                    });
            } else {
                if (1) {
                    var url = $rootScope.base_url + 'dashboard/blog/add';
                    $http.post(url, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined, 'Process-Data': false}
                    })
                        .then(function onSuccess(response) {
                            loadBlog();
                            $scope.newblog = {};
                            $scope.showform = false;

                        }, function onError(response) {
                            console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                            console.log(response.data);
                            $scope.validationError = response.data;
                            $scope.validationError.file = response.data;

                            if (response.status == 403) {
                                $scope.fileValidation.status = true;
                                $scope.fileValidation.msg = response.data.validation_error;
                            }
                        });
                } else {
                    $scope.validationError.file = false;
                }
            }
        };

        $scope.deleteBlog = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'dashboard/blog/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    var index = $scope.blogs.indexOf(item);
                                    $scope.blogs.splice(index, 1);
                                    loadBlog();
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };
   
        /****Modal***/

        $scope.animationsEnabled = true;

        $scope.open = function (blog, size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return blog;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);

