<?php
/**
 * 002_initial_schema.php
 * Date: 06/03/19
 * Time: 02:35 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Initial_schema extends CI_Migration {

    public function up()
    {

        /**
         * Table structure for table 'testimonials'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'description' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('testimonials');

        /**
         * Table structure for table 'brands'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('brands');


        /**
         * Table structure for table 'clients'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'designation' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'description' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'facebook_link' =>[
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ],
            'youtube_link' =>[
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ],
            'instagram_link' =>[
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ],
            'twitter_link' =>[
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('clients');
          

        /**
         * Table structure for table 'blogs'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'type' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('blogs');
        
        /**
         * Table structure for table 'ci_sessions'
         *
         **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'VARCHAR',
                'constraint' => 128,
            ],
            'ip_address' => [
                'type' => 'VARCHAR',
                'constraint' => 45,
            ],
            'timestamp' => [
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
                'default' => 0,
            ],
            'data' => [
                'type' => 'BLOB',
            ]
        ]);
        $this->dbforge->add_key(['id', 'ip_address'], TRUE);
        $this->dbforge->create_table('ci_sessions');
        $this->db->query("ALTER TABLE `ci_sessions` ADD KEY `ci_sessions_timestamp` (`timestamp`)");

    }

    public function down()
    {
        $this->dbforge->drop_table('testimonials', TRUE);
        $this->dbforge->drop_table('brands', TRUE);
        $this->dbforge->drop_table('clients',TRUE);
        $this->dbforge->drop_table('blogs',TRUE);
    }
}