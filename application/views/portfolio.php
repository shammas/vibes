
    <section class="wow fadeIn big-section pb-0">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-10 text-center text-lg-left wow fadeIn">
                    <span class="text-extra-large text-middle-line text-dark-gray d-block margin-five-bottom md-margin-15px-bottom md-text-middle-line font-weight-300">Vinvibes Technologies, digital marketing agency</span>
                    <h4 class="text-extra-dark-gray d-inline-block font-weight-300 md-width-80 sm-width-100">
                        We always stay with our clients and respect their business. We deliver <strong class="text-deep-pink">100%</strong> and provide instant response.
                    </h4>
                </div> 
            </div>
        </div>
    </section>
    <section class="wow fadeIn">
        <div class="container">
            <div class="row">
            <?php
            if (isset($clients) and $clients) {
                    foreach ($clients as $client) {
                    ?>
                    <div class="col-12 col-lg-3 col-md-6 team-block text-left team-style-1 margin-40px-bottom md-margin-seven-bottom sm-margin-30px-bottom wow fadeInRight">
                        <figure>
                            <div class="team-image sm-width-100">
                                <img src="<?php echo $client->url . $client->file_name;?>" alt="" class="grayimg">
                                <div class="overlay-content text-center d-flex align-items-end justify-content-center">
                                    <div class="icon-social-small padding-twelve-all">
                                        <span class="text-white-2 text-small d-inline-block m-0"><?php echo $client->description;?></span>
                                        <div class="separator-line-horrizontal-full bg-deep-pink margin-eleven-tb"></div>
                                        <a href="<?php echo $client->facebook_link;?>" target="_blank" class="text-white-2"><i class="fab fa-facebook-f"></i></a>
                                        <a href="<?php echo $client->youtube_link;?>" target="_blank" class="text-white-2"><i class="fab fa-youtube"></i></a>
                                        <a href="<?php echo $client->instagram_link;?>" target="_blank" class="text-white-2"><i class="fab fa-instagram"></i></a>
                                        <a href="<?php echo $client->twitter_link;?>" target="_blank" class="text-white-2"><i class="fab fa-twitter"></i></a>
                                    </div>
                                </div>
                                <div class="team-overlay bg-extra-dark-gray opacity8"></div>
                            </div>
                            <figcaption>
                                <div class="team-member-position margin-20px-top text-center">
                                    <div class="text-small font-weight-900 text-deep-pink text-uppercase"><?php echo $client->name;?></div>
                                    <div class="text-extra-small text-uppercase text-extra-dark-gray"><?php echo $client->designation;?></div>
                                </div>   
                            </figcaption>
                        </figure>
                    </div>
                    <?php 
                    }
                }
                ?>    
                
            </div> 
        </div>
    </section>
   