
    <section class="p-0 main-slider h-100 mobile-height wow fadeIn">
        <div class="swiper-full-screen swiper-container h-100 w-100 white-move">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="row m-0">
                        <div class="col-12 col-md-6 d-flex align-items-center bg-deep-pink height-700px sm-height-350px">
                            <div class="text-left padding-twelve-all sm-padding-five-all width-100">
                                <h6 class="title-large text-white-2 alt-font font-weight-600 letter-spacing-minus-3">we create <b>media influencer</b>  </h6>
                                <span class="text-large font-weight-300 margin-30px-top width-65 lg-width-75 md-width-90 text-white-2 d-block sm-margin-15px-top">We make Internet personalities.</span>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 cover-background height-700px sm-height-400px" style="background-image:url('img/slider-1.jpg');"></div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row m-0">
                        <div class="col-12 col-md-6 d-flex align-items-center bg-deep-pink height-700px sm-height-350px">
                            <div class="text-left padding-twelve-all sm-padding-five-all width-100">
                                <h6 class="title-large text-white-2 alt-font font-weight-600 letter-spacing-minus-3">let's see you in online</h6>
                                <span class="text-large font-weight-300 margin-30px-top width-65 lg-width-75 md-width-90 text-white-2 d-block sm-margin-15px-top">We are creative Digital Marketing company.</span>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 cover-background height-700px sm-height-400px" style="background-image:url('img/slider-2.jpg');">
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row m-0">
                        <div class="col-12 col-md-6 d-flex align-items-center bg-deep-pink height-700px sm-height-350px">
                            <div class="text-left padding-twelve-all sm-padding-five-all width-100">
                                <h6 class="title-large text-white-2 alt-font font-weight-600 letter-spacing-minus-3">we are providing more</h6>
                                <span class="text-large font-weight-300 margin-30px-top width-65 lg-width-75 md-width-90 text-white-2 d-block sm-margin-15px-top">We are providing more fans, followers, customers, like, share, comments etc.</span>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 cover-background height-700px sm-height-400px" style="background-image:url('img/slider-3.jpg');">
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-pagination swiper-pagination-square swiper-pagination-white swiper-full-screen-pagination"></div>
        </div>
    </section>
    <section class="wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4 text-center text-md-left sm-margin-seven-bottom wow fadeInUp last-paragraph-no-margin">
                    <div class="row m-0">
                        <div class="col-12 col-lg-3 col-md-4 pl-0 sm-no-padding-right">
                            <i class=" icon-video icon-extra-medium text-deep-pink float-none float-md-left sm-margin-15px-bottom position-relative top-minus3"></i>
                            <span class="separator-line-verticle-large margin-ten-right bg-deep-pink float-right align-top margin-two-top d-none d-md-inline-block"></span>
                        </div>
                        <div class="col-12 col-lg-9 col-md-8 md-no-padding-lr">
                            <span class="text-medium margin-four-bottom text-extra-dark-gray alt-font d-block">Youtube Channel Management</span>
                            <p class="width-90 lg-width-100">We Vinvibes technologies are providing services  in youtube channel management for Internet personalities and Video Blogging.</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 text-center text-md-left sm-margin-seven-bottom wow fadeInUp last-paragraph-no-margin" data-wow-delay="0.2s">
                    <div class="row m-0">
                        <div class="col-12 col-lg-3 col-md-4 pl-0 sm-no-padding-right">
                            <i class="icon-chat icon-extra-medium text-deep-pink float-none float-md-left sm-margin-15px-bottom position-relative top-minus3"></i>
                            <span class="separator-line-verticle-large margin-ten-right bg-deep-pink float-right align-top margin-two-top d-none d-md-inline-block"></span>
                        </div>
                        <div class="col-12 col-lg-9 col-md-8 md-no-padding-lr">
                            <h5 class="text-medium margin-four-bottom text-extra-dark-gray alt-font d-block">Social Media Marketing Management Services</h5>
                            <p class="width-90 lg-width-100">We at Vinvibes Technologies are promoting Social media Marketing Campaign using Facebook and Instagram.</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 text-center text-md-left wow fadeInUp last-paragraph-no-margin" data-wow-delay="0.4s">
                    <div class="row m-0">
                        <div class="col-12 col-lg-3 col-md-4 pl-0 sm-no-padding-right">
                            <i class="icon-megaphone icon-extra-medium text-deep-pink float-none float-md-left sm-margin-15px-bottom position-relative top-minus3"></i>
                            <span class="separator-line-verticle-large margin-ten-right bg-deep-pink float-right align-top margin-two-top d-none d-md-inline-block"></span>
                        </div>
                        <div class="col-12 col-lg-9 col-md-8 md-no-padding-lr">
                            <h5 class="text-medium margin-four-bottom text-extra-dark-gray alt-font d-block">Video Content</h5>
                            <p class="width-90 lg-width-100">Vinvibes Technologies is also focusing on giving services in the area of Graphical Designs and Digital Content Designing.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wow fadeIn bg-extra-dark-gray padding-55px-lr">
        <div class="container"> 
            <div class="row">
                <div class="col-12 col-xl-5 col-lg-6 d-flex align-items-center md-margin-five-bottom text-center text-lg-left sm-no-margin-top wow fadeIn" data-wow-delay="0.4s">
                    <div class="lg-padding-15px-lr sm-padding-five-lr sm-padding-ten-bottom w-100">
                        <span class="alt-font text-white d-block margin-10px-bottom">Vinvibes make Internet personalities</span>
                        <h6 class="text-light-gray alt-font width-90 md-width-100">Vinvibes is specialized in Video vloging and Branding.</h6>
                        <p class="width-85 lg-width-90 md-width-100 text-white-2">
                            We digital expertise and creativity to build something great together.
                            An effective Brand Identity is the one which customers associate with a high level of credibility and quality.
                            We help you create a Brand Identity that is unique, aesthetically pleasing and more importantly- has a personality which helps your audience to differentiate your brand from the rest.
                        </p>
                        <a href="services.html" class="btn btn-transparent-white btn-very-small border-radius-4"><i class="fas fa-play-circle icon-very-small margin-5px-right ml-0" aria-hidden="true"></i>Read More</a>
                    </div>
                </div>
                <div class="col-12 col-lg-3 offset-xl-1 text-center md-margin-five-bottom wow fadeIn">
                    <img src="img/home-1.jpg" alt="Vinvibes Technologies" class="w-100">
                </div>
                <div class="col-lg-3 text-left wow fadeIn" data-wow-delay="0.2s">
                    <div class="d-flex align-items-start flex-column justify-content-center text-white-2 bg-deep-pink padding-35px-lr lg-padding-15px-all md-padding-30px-all last-paragraph-no-margin h-100">
                        <div class="text-large margin-15px-bottom width-100">An effective social media targeting and promotions</div>
                        <p class="width-100">
                            We, Vinvibes, help you tackle your goal in this crowded online market and help you to engage your brand in all online communities. We publish content, we schedule the relevant strategies, and we make social interactions.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="swiper-slider-second swiper-container black-move">
                    <div class="swiper-wrapper">
                    <?php
                    if (isset($testimonials) and $testimonials) {
                            foreach ($testimonials as $testimonial) {
                            ?>
                            <div class="swiper-slide">
                                <div class="row justify-content-center m-0">
                                    <div class="col-12 col-lg-7 col-md-10">
                                        <div class="media d-block d-md-flex text-center text-md-left align-items-center padding-30px-lr lg-padding-15px-lr width-100">
                                            <img src="<?php echo $testimonial->url . $testimonial->file_name;?>" alt="Vinvibes Technologies" class="rounded-circle width-130px margin-50px-right lg-width-100px md-width-120px sm-width-100px sm-no-margin-right sm-margin-15px-bottom" />
                                            <div class="media-body last-paragraph-no-margin">
                                                <p><?php echo $testimonial->description;?></p>
                                                <span class="text-extra-dark-gray alt-font d-inline-block font-weight-900 text-uppercase text-small margin-15px-top">- <?php echo $testimonial->name;?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                            }
                        }
                        ?>    
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="p-0 wow fadeIn bg-deep-pink">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 cover-background md-height-500px sm-height-350px wow fadeIn" style="background-image:url('img/home-2.jpg');"><div class="sm-height-400px"></div></div>
                <div class="col-12 col-lg-6 d-flex align-items-center wow fadeInRight last-paragraph-no-margin text-center text-lg-left">
                    <div class="width-100 padding-55px-lr padding-60px-tb lg-padding-ten-all md-padding-90px-all sm-padding-40px-tb sm-no-padding-lr">
                        <span class="text-medium margin-20px-bottom d-block alt-font text-white-2">We’ll help you engage new audiences !</span>
                        <h4 class="alt-font text-white">Telling your story through the right mediums...</h4>
                        <p class="width-85 lg-width-100 text-light-gray">
                            We’ll help you engage new audiences, grow brand visibility online, improve social engagement and enhance conversion rates through video marketing. Video content has remarkable power. More than 75% of online consumers will trust content that is delivered to them visually. We Helping you to develop the right video marketing strategy to tell your brand story and engage your audiences. Going on location to shoot and produce broadcast-quality video content.
                        </p>
                        <a href="contact.html" class="btn btn-very-small btn-transparent-white text-small border-radius-4 lg-margin-15px-bottom margin-35px-top">Let's talk. Get in touch</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wow fadeIn">
        <div class="container text-center">
            <div class="swiper-slider-clients swiper-container black-move">
                <div class="swiper-wrapper">
                <?php
                if (isset($brands) and $brands) {
                    foreach ($brands as $brand) {
                    ?>
                    <div class="swiper-slide text-center"><img src="<?php echo $brand->url . $brand->file_name;?>" alt="<?php echo $brand->name;?>"></div>
                    <?php 
                    }
                }
                ?>     
                </div>
            </div>
        </div>    
    </section>
    