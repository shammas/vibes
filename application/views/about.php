
    <section class="wow fadeIn">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-4 text-center text-lg-left md-margin-40px-bottom sm-margin-30px-bottom wow fadeIn">
                    <h3 class="text-medium-gray alt-font font-weight-600 text-middle-line margin-ten-bottom md-margin-5px-bottom w-50 letter-spacing-minus-3 md-width-100 md-text-middle-line">01</h3>
                    <h5 class="text-uppercase abou text-deep-pink width-75 d-block mb-0 lg-width-90 md-width-100 sm-width-100">who</h5>
                </div>
                <div class="col-12 col-lg-4 col-md-6 sm-margin-30px-bottom wow fadeIn" data-wow-delay="0.2s">
                    <img class="padding-ten-right md-no-padding-right w-100" src="img/about-1.jpg" alt="">
                </div>
                <div class="col-12 col-lg-4 col-md-6 text-center text-md-left md-padding-eight-left sm-padding-15px-left wow fadeIn" data-wow-delay="0.4s">
                    <p class="text-large text-black font-weight-900">We always stay on the cutting edge of digital, so that our clients maintain their competitive advantage online.</p>
                    <p>
                        <b class="text-deep-pink">Vinvibes Technologies</b>, Digital marketing company started 10/10/2010. The most powerful online marketing tool you can see today is video. Today we are going through one of the biggest revolutions known as the communication revolution.
                    </p>
                    <p>
                        Online video isn’t just for teenagers anymore, it’s become the most important way to communicate for all of us and if you’re not visible online, you’re not in business.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="wow fadeIn">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-4 text-center text-lg-left md-margin-40px-bottom sm-margin-30px-bottom wow fadeIn">
                    <h3 class="text-medium-gray alt-font font-weight-600 text-middle-line margin-ten-bottom md-margin-5px-bottom w-50 letter-spacing-minus-3 md-width-100 md-text-middle-line">02</h3>
                    <h5 class="text-uppercase abou text-deep-pink width-75 d-block mb-0 lg-width-90 md-width-100 sm-width-100">we</h5>
                </div>
                <div class="col-12 col-lg-4 col-md-6 sm-margin-30px-bottom wow fadeIn" data-wow-delay="0.2s">
                    <img class="padding-ten-right md-no-padding-right w-100" src="img/about-1.jpg" alt="">
                </div>
                <div class="col-12 col-lg-4 col-md-6 text-center text-md-left md-padding-eight-left sm-padding-15px-left wow fadeIn" data-wow-delay="0.4s">
                    <p class="text-large text-black font-weight-900">We are social media marketing company who believe in new innovative ideas.Identities ,Experiences and our working strategies  are the key factors that make us differentiated from the other social media marketing companies in the market.</p>
                    <p>
                        In today’s current era, we all  have the choice to view the products or services offered by different organisations and business firms before buying the products/services. This is possible due to the high usage of internet marketing facility available today. 
                    </p>
                    <p>
                        We <b class="text-deep-pink">Vinvibes Technologies</b> is helping the traditional business firms and other professional organisations and instituitions in bringing up their products and services to the public thereby increasing their marketing space along with the increase in their income/profit. This is made possible through us <b class="text-deep-pink">Vinvibes Technologies</b> by the help of youtube channel management.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="wow fadeIn">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-4 text-center text-lg-left md-margin-40px-bottom sm-margin-30px-bottom wow fadeIn">
                    <h3 class="text-medium-gray alt-font font-weight-600 text-middle-line margin-ten-bottom md-margin-5px-bottom w-50 letter-spacing-minus-3 md-width-100 md-text-middle-line">03</h3>
                    <h5 class="text-uppercase abou text-deep-pink width-75 d-block mb-0 lg-width-90 md-width-100 sm-width-100">are</h5>
                </div>
                <div class="col-12 col-lg-4 col-md-6 sm-margin-30px-bottom wow fadeIn" data-wow-delay="0.2s">
                    <img class="padding-ten-right md-no-padding-right w-100" src="img/about-1.jpg" alt="">
                </div>
                <div class="col-12 col-lg-4 col-md-6 text-center text-md-left md-padding-eight-left sm-padding-15px-left wow fadeIn" data-wow-delay="0.4s">
                    <p class="text-large text-black font-weight-900">
                        We are creative perfectionists and this lets us go beyond existing framework in developing strategies, as well as in creativity & design.
                    </p>
                    <p>
                        Digital marketing has forever changed how companies attract customers and generate revenue. If you are considering hiring an online marketing company to help you boost your brand image, position your company in search rankings, or manage your social media presence, we <b class="text-deep-pink">Vinvibes Technologies</b> here to help.
                    </p>
                    <p>
                        <b class="text-deep-pink">Vinvibes Technologies</b> is home to the leading influencers from all social platforms - Youtube,Insta, facebook, twitter, blogger and others. It makes <b class="text-deep-pink">Vinvibes Technologies</b> the best platform for influencer marketing campaigns! Many leading brands trust our services.
                    </p>
                </div>
            </div>
        </div>
    </section>
