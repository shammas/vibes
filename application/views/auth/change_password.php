
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Inafcon Investments | Change Password</title>
    <link href="favicon.png" rel="shortcut icon" type="image/png">
    <link href="<?php echo base_url() . 'assets/dashboard/';?>font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/bootstrap.min.css" rel="stylesheet">
    
    <link href="<?php echo base_url(); ?>./assets/dashboard/css/style.css" rel="stylesheet">
    

</head> 

<body class="gray-bg">

   
      <h1><?php echo lang('change_password_heading');?></h1>

      <div id="infoMessage"><?php echo $message;?></div>

      <?php echo form_open("auth/change_password");?>

            <p>
                  <?php echo lang('change_password_old_password_label', 'old_password');?> <br />
                  <?php echo form_input($old_password);?>
            </p>

            <p>
                  <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label> <br />
                  <?php echo form_input($new_password);?>
            </p>

            <p>
                  <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?> <br />
                  <?php echo form_input($new_password_confirm);?>
            </p>

            <?php echo form_input($user_id);?>
            <p><?php echo form_submit('submit', lang('change_password_submit_btn'));?></p>

      <?php echo form_close();?>
 

<!-- Mainly scripts -->
<!-- <script src="<?php echo asset() . 'dashboard/';?>js/jquery-3.1.1.min.js"></script> -->
<!-- <script src="<?php echo asset() . 'dashboard/';?>js/bootstrap.min.js"></script> -->

<script src="<?php echo base_url(); ?>./assets/dashboard/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>./assets/dashboard/js/bootstrap.min.js"></script>

</body>

</html>
