
    <script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>
    <section class="p-0 wow fadeIn text-center text-md-left">
        <div class="container p-0">
            <div class="row no-gutters">
                <div class="col-12 col-xl-6 col-lg-4 cover-background" style="background-image: url('img/blogs-1.jpg')"><div class="md-height-500px sm-height-350px"></div></div>
                <div class="col-12 col-xl-3 col-lg-4 bg-deep-pink last-paragraph-no-margin text-center text-lg-left">
                    <div class="bg padding-70px-all lg-padding-40px-all md-padding-60px-all sm-padding-30px-all">
                        <h5 class="alt-font font-weight-700 text-white text-uppercase">Let's See You in.</h5>
                        <div class="separator-line-horrizontal-medium-thick bg-extra-dark-gray width-70 md-width-70 margin-25px-bottom md-margin-20px-bottom sm-width-50 mx-auto mx-lg-0 sm-margin-15px-bottom"></div>
                        <p class="width-95 lg-width-100 text-white-2">We are idea-driven, working with a strong focus on your products and services to the public thereby increasing your marketing space along with the increase income/profit.</p>
                    </div>
                </div>
                <div class="col-12 col-xl-3 col-lg-4 cover-background" style="background-image: url('img/blogs-2.jpg')"><div class="md-height-500px sm-height-350px"></div></div>
            </div>
        </div>
    </section>
    <section class="wow fadeIn">
        <div class="container">
            <div class="row">
            <?php
            if (isset($blogs) and $blogs) {
                    foreach ($blogs as $blog) {
                    ?>

                    <?php if($blog->type == "Facebook") {?>
                        <div class="col-md-4 ">
                            <div class="fb-post" 
                              data-href="<?php echo $blog->url;?>"
                              ></div>
                        </div>

                    <?php }elseif($blog->type == "Youtube") { ?>
                        <div class="col-md-4 ">
                           <iframe width="100%" height="315" src="https://www.youtube.com/embed/<?php echo $blog->youtube; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>

                    <?php }else { ?>
                        <div class="col-md-4 ">
                               <?php echo $blog->url;?> 
                        </div>
                     <?php } ?>   

                    <?php 
                   } 
                }
                ?> 
            </div>
            <!-- <div class=" text-center margin-100px-top md-margin-50px-top wow fadeInUp">
                <div class="pagination text-small text-uppercase text-extra-dark-gray">
                    <ul class="mx-auto">
                        <li><a href="#"><i class="fas fa-long-arrow-alt-left margin-5px-right d-none d-md-inline-block"></i> Prev</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">Next <i class="fas fa-long-arrow-alt-right margin-5px-left d-none d-md-inline-block"></i></a></li>
                    </ul>
                </div>
            </div> -->
        </div>
    </section> 
    