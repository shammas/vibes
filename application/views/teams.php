
    <section class="p-0 bg-light-gray wow fadeIn">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-6 text-center align-self-end">
                    <img style="filter: grayscale(90%);" src="img/Vineesh.jpg" alt="">
                </div>
                <div class="col-12 col-lg-5 col-md-10 text-center text-lg-left offset-lg-1 mx-auto mr-lg-0 lg-padding-50px-tb">
                    <i class="fas fa-quote-left text-deep-pink icon-medium margin-15px-bottom"></i>
                    <h5 class="text-extra-dark-gray alt-font text-uppercase font-weight-700">The first rule of social media is that everything changes all the time.</h5>
                    <p class="width-90 md-width-100">
                        I use social media as an idea generator, trend mapper and strategic compass for all of our online business ventures. Social media is not just an activity; it is an investment of valuable time and resources. Surround yourself with people who not just support you and stay with you, but inform your thinking about ways to WOW your online presence
                    </p>
                    <span class="text-extra-dark-gray text-large d-block margin-50px-top alt-font font-weight-600 sm-margin-20px-top">Vineesh</span>
                    <span class="d-block">From the desk of Director</span>
                </div>
            </div>
        </div>
    </section>
    <section class="wow fadeIn">
        <div class="container">
            <div class="row">
                <!-- start team item -->
                <div class="col-12 col-lg-3 col-md-6 team-block text-left team-style-1 margin-40px-bottom md-margin-seven-bottom sm-margin-30px-bottom wow fadeInRight">
                    <figure>
                        <div class="team-image sm-width-100">
                            <img src="img/Vaiga Vineesh.jpg" alt="">
                            <div class="overlay-content text-center d-flex align-items-center justify-content-center">
                                <div class="icon-social-small">
                                    <a href="http://www.facebook.com" class="text-white-2" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                    <a href="http://www.instagram.com" class="text-white-2" target="_blank"><i class="fab fa-instagram"></i></a>
                                    <a href="http://www.plus.google.com" class="text-white-2" target="_blank"><i class="fab fa-linkedin"></i></a>
                                    <a href="http://www.twitter.com" class="text-white-2" target="_blank"><i class="fab fa-twitter"></i></a>
                                </div>
                            </div>
                            <div class="team-overlay bg-extra-dark-gray opacity8"></div>
                        </div>
                        <figcaption>
                            <div class="team-member-position margin-20px-top text-center">
                                <div class="text-small font-weight-700 text-extra-dark-gray text-uppercase">Vaiga Vineesh</div>
                                <div class="text-extra-small text-uppercase text-deep-pink">Consultant & Influencer</div>
                            </div>   
                        </figcaption>
                    </figure>
                </div>
                <!-- start team item -->
                <div class="col-12 col-lg-3 col-md-6 team-block text-left team-style-1 margin-40px-bottom md-margin-seven-bottom sm-margin-30px-bottom wow fadeInRight">
                    <figure>
                        <div class="team-image sm-width-100">
                            <img src="img/Suhail.jpg" alt="">
                            <div class="overlay-content text-center d-flex align-items-center justify-content-center">
                                <div class="icon-social-small">
                                    <a href="http://www.facebook.com" class="text-white-2" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                    <a href="http://www.instagram.com" class="text-white-2" target="_blank"><i class="fab fa-instagram"></i></a>
                                    <a href="http://www.plus.google.com" class="text-white-2" target="_blank"><i class="fab fa-linkedin"></i></a>
                                    <a href="http://www.twitter.com" class="text-white-2" target="_blank"><i class="fab fa-twitter"></i></a>
                                </div>
                            </div>
                            <div class="team-overlay bg-extra-dark-gray opacity8"></div>
                        </div>
                        <figcaption>
                            <div class="team-member-position margin-20px-top text-center">
                                <div class="text-small font-weight-700 text-extra-dark-gray text-uppercase">Suhail PK</div>
                                <div class="text-extra-small text-uppercase text-deep-pink">Channel Development Manager</div>
                            </div>   
                        </figcaption>
                    </figure>
                </div>
                <!-- start team item -->
                <div class="col-12 col-lg-3 col-md-6 team-block text-left team-style-1 margin-40px-bottom md-margin-seven-bottom sm-margin-30px-bottom wow fadeInRight">
                    <figure>
                        <div class="team-image sm-width-100">
                            <img src="img/Nuhman.jpg" alt="">
                            <div class="overlay-content text-center d-flex align-items-center justify-content-center">
                                <div class="icon-social-small">
                                    <a href="http://www.facebook.com" class="text-white-2" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                    <a href="http://www.instagram.com" class="text-white-2" target="_blank"><i class="fab fa-instagram"></i></a>
                                    <a href="http://www.plus.google.com" class="text-white-2" target="_blank"><i class="fab fa-linkedin"></i></a>
                                    <a href="http://www.twitter.com" class="text-white-2" target="_blank"><i class="fab fa-twitter"></i></a>
                                </div>
                            </div>
                            <div class="team-overlay bg-extra-dark-gray opacity8"></div>
                        </div>
                        <figcaption>
                            <div class="team-member-position margin-20px-top text-center">
                                <div class="text-small font-weight-700 text-extra-dark-gray text-uppercase">Nooman Shibli</div>
                                <div class="text-extra-small text-uppercase text-deep-pink">Director Of Content</div>
                            </div>   
                        </figcaption>
                    </figure>
                </div>
                <!-- start team item -->
                <div class="col-12 col-lg-3 col-md-6 team-block text-left team-style-1 margin-40px-bottom md-margin-seven-bottom sm-margin-30px-bottom wow fadeInRight">
                    <figure>
                        <div class="team-image sm-width-100">
                            <img src="img/Manu.jpg" alt="">
                            <div class="overlay-content text-center d-flex align-items-center justify-content-center">
                                <div class="icon-social-small">
                                    <a href="http://www.facebook.com" class="text-white-2" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                    <a href="http://www.instagram.com" class="text-white-2" target="_blank"><i class="fab fa-instagram"></i></a>
                                    <a href="http://www.plus.google.com" class="text-white-2" target="_blank"><i class="fab fa-linkedin"></i></a>
                                    <a href="http://www.twitter.com" class="text-white-2" target="_blank"><i class="fab fa-twitter"></i></a>
                                </div>
                            </div>
                            <div class="team-overlay bg-extra-dark-gray opacity8"></div>
                        </div>
                        <figcaption>
                            <div class="team-member-position margin-20px-top text-center">
                                <div class="text-small font-weight-700 text-extra-dark-gray text-uppercase">Arun PK</div>
                                <div class="text-extra-small text-uppercase text-deep-pink">Creative Director</div>
                            </div>   
                        </figcaption>
                    </figure>
                </div>
            </div> 
            <div class="row  padding-seventeen-lr lg-padding-ten-lr sm-padding-15px-lr margin-20px-top">
                <div class="col-12 col-lg-12">
                    <h5 class="text-extra-dark-gray alt-font font-weight-600">Content and Voice</h5>
                </div>
                <div class="col-12 col-lg-6">
                    <ul class="p-0 list-style-4 list-style-color">
                        <li class="fs20">Vineesh</li>
                        <li class="fs20">Sajithra</li>
                        <li class="fs20">Hari</li>
                        <li class="fs20">Viji</li>
                    </ul>
                </div>
                <div class="col-12 col-lg-6">
                    <ul class="p-0 list-style-4 list-style-color">
                        <li class="fs20">Mashoodh</li>
                        <li class="fs20">Saritha</li>
                        <li class="fs20">Vaiga</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="pb-0 bg-deep-pink wow fadeIn">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-8 text-center">
                    <div class="alt-font text-white margin-10px-bottom text-uppercase text-small">idea factory ccd society</div>
                    <h5 class="alt-font text-white font-weight-600">Thanks to All of You</h5>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 text-center no-padding-left no-padding-right">
                    <img src="img/ideafactory.jpg" alt=""/>
                </div>
            </div>
        </div>
    </section>
    