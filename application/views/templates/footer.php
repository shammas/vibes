<section class="wow fadeIn border-bottom border-color-extra-medium-gray pt-0">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-10 wow fadeIn text-center text-lg-left">
                    <span class="text-extra-large text-middle-line text-dark-gray d-block margin-five-bottom md-margin-15px-bottom md-text-middle-line font-weight-300">Great Video Marketing Agency</span>
                    <img class="footer-logo" src="img/logo.png" data-rjs="img/logo.png" alt="Vinvibes Technologies">
                    <h5 class="text-deep-pink d-block font-weight-300 mb-0 md-width-80 sm-width-100 mx-md-auto">
                        Door No.33/1820-J,  M3 Properties,<br>
                        Saithalikutty Bypass Road, Kacheripadi,<br>
                        Manjeri-676121, Malappuram,<br>
                        Kerala - IND
                    </h5>
                </div>
                <div class="col-12 col-lg-2 wow fadeInRightadmin">
                    <div class="widget-title alt-font text-deep-pink fs25 font-weight-300 margin-40px-top">Quick Links</div>
                    <ul class="list-unstyled">
                        <li><a class="qlinks" href="about">About Us</a></li>
                        <li><a class="qlinks" href="services">Our Services</a></li>
                        <li><a class="qlinks" href="portfolio">Our Clients</a></li>
                        <li><a class="qlinks" href="teams">Our Teams</a></li>
                        <li><a class="qlinks" href="blogs">Our Webinar</a></li>
                        <li><a class="qlinks" href="contact">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <footer class="footer-strip padding-15px-tb">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 footer-social">
                    <a class="social-link facebook" href="#" target="_blank"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 footer-social">
                    <a class="social-link instagram" href="#" target="_blank"><i class="fab fa-instagram mr-0" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 footer-social">
                    <a class="social-link youtube" href="#" target="_blank"><i class="fab fa-youtube mr-0" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 footer-social">
                    <a class="social-link linkedin" href="#" target="_blank"><i class="fab fa-linkedin"></i></a>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 footer-social">
                    <a class="social-link dribbble" href="#" target="_blank"><i class="fab fa-dribbble"></i></a>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 footer-social">
                    <a class="social-link twitter" href="#" target="_blank"><i class="fab fa-twitter"></i></a>
                </div>
            </div>
        </div>
    </footer>

    <span class="enq">Enquiry : +91 8590 991 111</span>    
    <a class="scroll-top-arrow" href="javascript:void(0);"><i class="ti-arrow-up"></i></a>
    <!-- javascript libraries -->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/modernizr.js"></script>
    <script type="text/javascript" src="js/bootstrap.bundle.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/skrollr.min.js"></script>
    <script type="text/javascript" src="js/smooth-scroll.js"></script>
    <script type="text/javascript" src="js/jquery.appear.js"></script>
    <!-- menu navigation -->
    <script type="text/javascript" src="js/bootsnav.js"></script>
    <script type="text/javascript" src="js/jquery.nav.js"></script>
    <!-- animation -->
    <script type="text/javascript" src="js/wow.min.js"></script>
    <!-- page scroll -->
    <script type="text/javascript" src="js/page-scroll.js"></script>
    <!-- swiper carousel -->
    <script type="text/javascript" src="js/swiper.min.js"></script>
    <!-- counter -->
    <script type="text/javascript" src="js/jquery.count-to.js"></script>
    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.js"></script>
    <!-- magnific popup -->
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <!-- portfolio with shorting tab -->
    <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
    <!-- images loaded -->
    <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
    <!-- pull menu -->
    <script type="text/javascript" src="js/classie.js"></script>
    <script type="text/javascript" src="js/hamburger-menu.js"></script>
    <!-- counter  -->
    <script type="text/javascript" src="js/counter.js"></script>
    <!-- fit video  -->
    <script type="text/javascript" src="js/jquery.fitvids.js"></script>
    <!-- justified gallery  -->
    <script type="text/javascript" src="js/justified-gallery.min.js"></script>
    <!-- retina -->
    <script type="text/javascript" src="js/retina.min.js"></script>
    <!-- setting -->
    <script type="text/javascript" src="js/main.js"></script>
</body>
</html>