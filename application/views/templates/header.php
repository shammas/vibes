<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>Vinvibes | Social Media Influencer</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
    <meta name="author" content="Cloudbery Solutions | Shammas Pk">
    <meta property="og:title" name="title" content="Vinvibes | Social Media Influencing Agency India" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://www.vinvibes.in" />
    <meta name="description" content="Social media promotor Agency headquartered in Kerala helping your business to reach out to the people who need you through digital media">
    <meta name="keywords" content="vinvibes youtube management company in india, vinvibes youtube management company in kerala, vinvibes youtube management company in uae or dubai, vinvibes youtube management company in kozhikode or malappuram, vinvibes youtube management agency in kerala, vinvibes best youtube management company in kerala, vinvibes social media agency in malabar, kozhikode, malappuram, keralam uae dubai and india, vinvibes facebook video marketing in kerala, vinvibes video content marketing company in kerala, vinvibes video content makers in kerala, vinvibes best video strategist in malappuram and kozhikode, vinvibes explainer video in kerala, vinvibes screen casting and tutorial videos in malabar, kozhikode and kerala, vinvibes video advertisement in kozhikode, kerala and malappuram">
    <link rel="shortcut icon" href="img/favicon.png">
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/et-line-icons.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/swiper.min.css">
    <link rel="stylesheet" href="css/justified-gallery.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <link rel="stylesheet" href="css/bootsnav.css">
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/responsive.css" />
</head>
<body class="box-layout box-layout-md">
    <header>
        <nav class="navbar full-width-pull-menu navbar-top-scroll navbar-fixed-top no-border-top no-transition">
            <div class="container nav-header-container height-70px sm-height-70px sm-padding-15px-lr">
                <div class="col-auto pl-0">
                    <a href="index" title="Vinvibes Technologies" class="logo"><img src="img/logo.png" data-rjs="img/logo.png" alt="Vinvibes Technologies"></a>
                </div>
                <div class="col-auto pr-0">
                    <div class="menu-wrap full-screen d-flex">
                        <div class="col-md-6 p-0 d-none d-md-block"> 
                            <div class="cover-background full-screen" style="background-image:url('img/menu-background-1.png')">     
                                <div class="opacity-medium bg-extra-dark-gray"></div>
                                <div class="position-absolute height-100 width-100 text-center">
                                    <div class="d-table height-100 width-100">
                                        <div class="d-table-cell height-100 width-100 vertical-align-middle position-relative">
                                            <a href="index"><img alt="Vinvibes Technologies" src="img/nav-logo.png" data-rjs="img/nav-logo.png"></a>
                                            <div class="position-absolute bottom-50 text-center width-100 margin-30px-bottom">
                                                <div class="text-small text-extra-medium-gray">
                                                    &copy; <span id="year"></span> Vinvibes is Proudly Powered by <a href="http://cloudbery.com/" target="_blank" title="www.cloudbery.com"><img src="img/cloudbery.png"></a>.
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                                            
                                </div>                                                    
                            </div>
                        </div>
                        <div class="col-md-6 p-0 bg-white full-screen">
                            <div class="position-absolute height-100 width-100 overflow-auto">
                                <div class="d-table height-100 width-100 bg-extra-dark-gray">
                                    <div class="d-table-cell height-100 width-100 vertical-align-middle padding-fourteen-lr alt-font link-style-2 md-padding-seven-lr sm-padding-15px-lr">
                                        <ul class="font-weight-600 sm-no-padding-left">
                                            <li><a href="index">Welcome</a></li>
                                            <li><a href="about">Who We Are</a></li>
                                            <li><a href="services">Vibes Service</a></li>
                                            <li><a href="portfolio">Vibes Family</a></li>
                                            <li><a href="teams">Team Vibes</a></li>
                                            <li><a href="blogs">Vibes Diary</a></li>
                                            <li><a href="contact">Get In Touch</a></li>
                                        </ul>
                                        <span class="padding-35px-left text-large text-middle-line font-weight-300 text-white-2 d-block">Call Us : +91 8590 991 111</span>
                                        <div class="margin-ten-top padding-35px-left sm-no-padding-left">
                                            <div class="icon-social-medium margin-three-bottom">
                                                <a href="https://www.facebook.com/" target="_blank" class="text-white text-deep-pink-hover margin-one-lr">
                                                    <i class="fab fa-facebook-f" aria-hidden="true"></i>
                                                </a>
                                                <a href="https://dribbble.com/" target="_blank" class="text-white text-deep-pink-hover margin-one-lr">
                                                    <i class="fab fa-instagram" aria-hidden="true"></i>
                                                </a>
                                                <a href="https://www.youtube.com/" target="_blank" class="text-white text-deep-pink-hover margin-one-lr">
                                                    <i class="fab fa-youtube" aria-hidden="true"></i>
                                                </a>
                                                <a href="https://twitter.com/" target="_blank" class="text-white text-deep-pink-hover margin-one-lr">
                                                    <i class="fab fa-twitter" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="close-button-menu" id="close-button"></button>
                        </div>
                    </div>
                    <button class="navbar-toggler mobile-toggle" type="button" id="open-button" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
            </div>
        </nav> 
    </header>