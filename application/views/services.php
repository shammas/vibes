
    <section class="wow fadeIn p-0 parallax sm-background-image-center" data-stellar-background-ratio="0.5" style="background-image:url('img/services-1.jpg');">
        <div class="opacity-extra-medium bg-black"></div>
        <div class="container-fluid padding-thirteen-lr small-screen sm-padding-15px-lr">
            <div class="row h-100">
                <div class="position-relative h-100 w-100">
                    <div class="slider-typography">
                        <div class="slider-text-middle-main">
                            <div class="slider-text-bottom">
                                <div class="col-12 text-center">
                                    <h4 class="text-white-2 alt-font font-weight-300 width-60 mx-auto margin-ten-bottom lg-margin-fifteen-bottom lg-width-80 md-margin-twenty-bottom sm-width-100 sm-margin-100px-bottom">A <strong>Social Media</strong> Marketing Company crafting beautiful <strong>experiences</strong>.</h4>
                                </div>
                            </div>
                            <div class="down-section text-center">
                                <a href="#services" class="inner-link"><i class="ti-arrow-down icon-medium text-deep-pink"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wow fadeIn bg-deep-pink">
        <div class="container-fluid padding-five-half-lr lg-padding-six-lr sm-padding-15px-lr">
            <div class="row align-items-center">
                <div class="col-12 col-lg-5 md-margin-nine-bottom sm-margin-50px-bottom text-center text-md-left wow fadeIn last-paragraph-no-margin">
                    <span class="text-small text-uppercase text-white-2 opacity7 font-weight-500 letter-spacing-1 alt-font margin-10px-bottom md-margin-5px-bottom d-block">
                        Vinvibes Digital marketing company
                    </span>
                    <h4 class="text-white-2 font-weight-300 alt-font w-100">A digital studio crafting <strong>beautiful experiences</strong></h4>
                    <p class="width-80 text-white-2 lg-width-90 md-width-100">
                        <b class="text-black">Vinvibes technologies</b> is a Digital media marketing company. The most powerful online marketing tool you can see today is video. Today we are going through one of the biggest revolutions known as the communication revolution. Online video isn’t just for teenagers anymore, it’s become the most important way to communicate for all of us and if you’re not visible online, you’re not in business.
                    </p>
                    <p class="width-80 text-white-2 lg-width-90 md-width-100">
                        Vinvibes technologies maintain full transparency so you can see first-hand how our efforts are increasing your online revenue. Our custom, in-depth reporting measures include
                    </p>
                    <a href="#" class="btn btn-black margin-35px-top btn-small md-margin-25px-top">Our Clients Say's</a>
                </div> 
                <div class="col-12 col-lg-7 text-center text-md-left wow fadeIn">
                    <div class="row">
                        <div class="col-12 col-md-6 margin-eight-bottom md-margin-30px-bottom last-paragraph-no-margin">
                            <div class="position-relative">
                                <i class="icon-camera text-white-2 icon-extra-medium margin-20px-bottom md-margin-15px-bottom"></i>
                                <div class="feature-content">
                                    <div class="text-white-2 text-medium alt-font font-weight-600 sm-margin-5px-bottom">Youtube Personalities</div>
                                    <p class="text-white-2 width-80 lg-width-90 sm-width-100">
                                        We Vinvibes technologies are providing services  in you tube channel management  for doctors,  public speakers, motivators, etc.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 margin-eight-bottom md-margin-30px-bottom last-paragraph-no-margin">
                            <div class="position-relative">
                                <i class="icon-toolbox text-white-2 icon-extra-medium margin-20px-bottom md-margin-15px-bottom"></i>
                                <div class="feature-content">
                                    <div class="text-white-2 text-medium alt-font font-weight-600 sm-margin-5px-bottom">Video Blogging for Business</div>
                                    <p class="text-white-2 width-80 lg-width-90 sm-width-100">
                                        Professional organisations and instituitions in bringing up their products and services to the public thereby increasing their marketing.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 sm-margin-30px-bottom last-paragraph-no-margin">
                            <div class="position-relative">
                                <i class="icon-wallet text-white-2 icon-extra-medium margin-20px-bottom md-margin-15px-bottom"></i>
                                <div class="feature-content">
                                    <div class="text-white-2 text-medium alt-font font-weight-600 sm-margin-5px-bottom">Social Media Content</div>
                                    <p class="text-white-2 width-80 lg-width-90 sm-width-100">
                                        we promoting Social media Marketing Campaign using Facebook and Instagram. By this campaign, mainly focusing at making the Reach account at the maximum level. 
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 last-paragraph-no-margin">
                            <div class="position-relative">
                                <i class="icon-gift text-white-2 icon-extra-medium margin-20px-bottom md-margin-15px-bottom"></i>
                                <div class="feature-content">
                                    <div class="text-white-2 text-medium alt-font font-weight-600 sm-margin-5px-bottom">Digital Services & Courses</div>
                                    <p class="text-white-2 width-80 lg-width-90 sm-width-100">
                                        We also focusing on giving services in the area of Graphical Designs and Digital Content Designing. Influencers are given a  special platform for developing their growth
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wow fadeIn" id="services">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4 feature-box-1 sm-margin-30px-bottom wow fadeIn last-paragraph-no-margin">
                    <div class="d-flex align-items-center margin-15px-bottom alt-font">
                        <h3 class="char-value text-deep-pink letter-spacing-minus-1 text-medium-gray font-weight-600 mb-0">01.</h3>
                        <span class="text-large text-extra-dark-gray line-height-22 padding-20px-left width-100">Youtube Personalities </span>
                    </div>
                    <p class="width-90 md-width-100">
                        We Vinvibes technologies are providing services  in you tube channel management  for doctors,  public speakers, motivators, business consultants, teachers, chartered accountants, architects and other professionals. For Professionals who are selling their skills and for those who want to  build to their own brand, youtube is one of the best choices.By maintaining a youtube channel, you can improve your public relations skill along with a consistent growth in business and social life.
                    </p>
                </div>
                <div class="col-12 col-md-4 feature-box-1 sm-margin-30px-bottom wow fadeIn last-paragraph-no-margin" data-wow-delay="0.2s">
                    <div class="d-flex align-items-center margin-15px-bottom alt-font">
                        <h3 class="char-value text-deep-pink letter-spacing-minus-1 text-medium-gray font-weight-600 mb-0">02.</h3>
                        <span class="text-large text-extra-dark-gray line-height-22 padding-20px-left width-100">Video Blogging for Business  </span>
                    </div>
                    <p class="width-90 md-width-100">
                        Video Blogging is an  upcoming revolution in todays world.Comparing with the traditional way of advertising , individuals and business firms are focusing on a smart way of growth than the traditional way of hardwork.For Firms like hospitals, resorts, institutions, theme parks, travel and tourisum sector, real estate  etc. We Vinvibes Technologies are helping the business firms and organisations in achieving their growth using the youtube channel management.
                    </p>
                </div>
                <div class="col-12 col-md-4 feature-box-1 wow fadeIn last-paragraph-no-margin" data-wow-delay="0.4s">
                    <div class="d-flex align-items-center margin-15px-bottom alt-font">
                        <h3 class="char-value text-deep-pink letter-spacing-minus-1 text-medium-gray font-weight-600 mb-0">03.</h3>
                        <span class="text-large text-extra-dark-gray line-height-22 padding-20px-left width-100">Marketing videos</span>
                    </div>
                    <p class="width-90 md-width-100">
                        Video Resume - Create a rejection-proof video resume in minutes with Vinvibes Technologies.  A video resume can show employers your ability to enhance their company and fit their business culture before they even pick up your paper resume. It also sets you apart from the other candidates who have no video to support their application. We Vinvibes Technologies are providing the Video Resume matching to each person depending on their skills and strengths.
                    </p>
                </div>
            </div>
        </div>
    </section>  
    <section class="p-0 wow fadeIn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-lg-4 col-md-6 d-flex align-items-center bg-black feature-box-1 sm-no-padding-lr text-center text-md-left">
                    <div class="padding-eighteen-all lg-padding-ten-all sm-padding-30px-tb sm-padding-20px-lr width-100">
                        <h6 class="text-light-gray alt-font">Social media content making</h6>
                        <p class="m-0 text-light-gray">
                            <b class="text-deep-pink">Facebook & Instagram content campaign</b> – we at Vinvibes Technologies are promoting Social media Marketing Campaign using Facebook and Instagram. By this campaign, we are mainly focusing at making the Reach of each FB page Or instagram account at the maximum level.
                            <br/>
                            <b class="text-deep-pink">Other Services</b> - We at Vinvibes Technologies are also providing marketing assistance in Google Services, Wattsap marketing, Twitter etc
                        </p>
                    </div>
                </div>
                <div class="col-12 col-lg-4 col-md-6 d-flex align-items-center bg-extra-dark-gray feature-box-1 sm-no-padding-lr text-center text-md-left">
                    <div class="padding-eighteen-all lg-padding-ten-all sm-padding-30px-tb sm-padding-20px-lr width-100">
                        <h6 class="text-light-gray alt-font">Digital services & Courses</h6>
                        <p class="m-0 text-white">
                            <b class="text-deep-pink">Graphical Design works and Digital Content Designing</b> - We also focusing on giving services in the area of Graphical Designs and Digital Content Designing.
                            <br/>
                            <b>
                                <b class="text-deep-pink">Online courses, Workshops, Influencer’s forum</b> – We also providing a huge platform for learning different types of online courses related to Social Media Marketing along with One Day Workshops. Influencers are given a  special platform for creating and developing their growth through us.
                            </b>
                        </p>
                    </div>
                </div>
                <div class="col-12 col-lg-4 d-flex cover-background feature-box-1 md-height-450px sm-height-350px" style="background: url(img/services-2.jpg)"></div>
            </div>
        </div>
    </section>
    <section class="wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4 feature-box-1 sm-margin-30px-bottom wow fadeIn last-paragraph-no-margin">
                    <div class="d-flex align-items-center margin-15px-bottom alt-font">
                        <h3 class="char-value text-deep-pink letter-spacing-minus-1 text-medium-gray font-weight-600 mb-0">04.</h3>
                        <span class="text-large text-extra-dark-gray line-height-22 padding-20px-left width-100">Explainer video making</span>
                    </div>
                    <p class="width-90 md-width-100">
                        Explainer videos are otherwise known as digital sales man. They are short online marketing videos used to explain a company’s or business organisation’s product or service. With the use of explainer videos, one can market their product or service in a better and professional way rather than using the traditional way of marketing.
                        <br/>
                        We Vinvibes Technologies are providing the Video Resume matching to each person depending on their skills and strengths.
                    </p>
                </div>
                <div class="col-12 col-md-4 feature-box-1 sm-margin-30px-bottom wow fadeIn last-paragraph-no-margin" data-wow-delay="0.2s">
                    <div class="d-flex align-items-center margin-15px-bottom alt-font">
                        <h3 class="char-value text-deep-pink letter-spacing-minus-1 text-medium-gray font-weight-600 mb-0">05.</h3>
                        <span class="text-large text-extra-dark-gray line-height-22 padding-20px-left width-100">Video Advertisement</span>
                    </div>
                    <p class="width-90 md-width-100">
                        We Vinvibes Technologies are also providing services in the area of video graphics ads, social media short ads, audio visual treatment, 2 d animation videos etc. <br/><b class="text-deep-pink">Corporate video</b> – This refers to any type of non-advertisement based video content created for and commissioned by a business, company, corporation, or organization. This is mainly done for the promotion of the concerned business organisation.
                    </p>
                </div>
                <div class="col-12 col-md-4 feature-box-1 wow fadeIn last-paragraph-no-margin" data-wow-delay="0.4s">
                    <div class="d-flex align-items-center margin-15px-bottom alt-font">
                        <h3 class="char-value text-deep-pink letter-spacing-minus-1 text-medium-gray font-weight-600 mb-0">06.</h3>
                        <span class="text-large text-extra-dark-gray line-height-22 padding-20px-left width-100">Screen casting videos</span>
                    </div>
                    <p class="width-90 md-width-100">
                        A screencast is a digital recording of computer screen output. This is also known as a video screen capture or a screen recording, It often contains audio narration.  This includes tutorials ,mobile apps tutorials etc. <br/><b class="text-deep-pink">Documentaries</b> – A documentary film is a non-fictional, motion picture intended to "document reality, primarily for the purposes of instruction, education, or maintaining a historical record. 
                    </p>
                </div>
            </div>
        </div>
    </section> 
    