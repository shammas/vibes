
    <section class="wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 last-paragraph-no-margin wow fadeInLeft">
                    <div class="padding-ten-all bg-extra-dark-gray border-radius-6 lg-padding-seven-all sm-padding-30px-all h-100 text-center text-lg-left">
                        <img src="img/contact.jpg" alt="" class="border-radius-6 margin-35px-bottom sm-margin-30px-bottom">
                        <span class="text-large font-weight-600 alt-font text-white margin-5px-bottom d-block">Let's plan for a new project?</span>
                        <p class="text-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry, Lorem Ipsum has been the standard dummy text.</p>
                    </div>
                </div>
                <div class="col-12 col-lg-6 md-margin-30px-bottom wow fadeInRight">
                    <div class="padding-fifteen-all bg-deep-pink border-radius-6 lg-padding-seven-all sm-padding-30px-all h-100">
                        <span class="text-white alt-font text-large font-weight-600 margin-25px-bottom d-block">Ready to get started?</span> 
                        <form id="contact-form" action="javascript:void(0)" method="post">
                            <div>
                                <div id="success-contact-form" class="mx-0"></div>
                                <input type="text" name="name" id="name" placeholder="Name*" class="border-radius-4 bg-white medium-input">
                                <input type="text" name="email" id="email" placeholder="E-mail*" class="border-radius-4 bg-white medium-input">
                                <input type="text" name="subject" id="subject" placeholder="Subject" class="border-radius-4 bg-white medium-input">
                                <textarea name="comment" id="comment" placeholder="Your Message" rows="5" class="border-radius-4 bg-white medium-textarea"></textarea>
                                <button id="contact-us-button" type="submit" class="btn btn-small border-radius-4 btn-dark-gray">send message</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    