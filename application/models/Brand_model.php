<?php
/**
 * Brand_model.php
 * Date: 26/08/2020
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Brand_model extends MY_Model
{
	public $table = 'brands';

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}