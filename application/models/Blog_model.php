<?php
/**
 * Blog_model.php
 * Date: 26/08/2020
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Blog_model extends MY_Model
{
	public $table = 'blogs';

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}