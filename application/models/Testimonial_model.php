<?php
/**
 * Testimonial_model.php
 * Date: 26/08/2020
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Testimonial_model extends MY_Model
{
	public $table = 'testimonials';

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}