<?php
/**
 * Client_model.php
 * Date: 26/08/2020
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Client_model extends MY_Model
{

	public $table = 'clients';

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}