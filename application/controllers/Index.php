<?php
/**
 * Index.php
 * Date: 26/08/2020
 * Time: 10:03 AM
 */

class Index extends CI_Controller { 
	 
        protected $header = 'templates/header';
        protected $footer = 'templates/footer';

        public function __construct()
        {
            parent::__construct();

            $this->load->model('Testimonial_model', 'testimonial');
            $this->load->model('Brand_model', 'brand');
            $this->load->model('Client_model', 'client');
            $this->load->model('Blog_model', 'blog');
        }

        protected $current = '';

        public function index()
        {
            $data['testimonials'] = $this->testimonial->order_by('id','desc')->get_all();
            $data['brands'] = $this->brand->order_by('id','desc')->get_all();
           
            $this->current = 'index';
            $this->load->view($this->header, ['current' => $this->current]);
            $this->load->view('index',$data);
            $this->load->view($this->footer);
        }

        public function about()
        {
            $this->current = 'about';
            $this->load->view($this->header, ['current' => $this->current]);
            
            $this->load->view('about');
            $this->load->view($this->footer);
        }

        public function blogs()
        {
            $data['blogs'] = $this->blog->order_by('id','desc')->get_all();
            if($data['blogs']){
                foreach ($data['blogs'] as $key => $value) {
                    $array = array("https:" , "/" , "youtu.be", "www.youtube.com" ,"watch?v=");
                    $value->youtube = str_replace($array, "", $value->url);
                }
            }
            
            $this->current = 'blogs';
            $this->load->view($this->header, ['current' => $this->current]);
            
            $this->load->view('blogs', $data);
            $this->load->view($this->footer);
        }

        public function contact() 
        {
            $this->current = 'contact';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('contact');
            $this->load->view($this->footer);
        }

        public function portfolio()
        {
            $data['clients'] = $this->client->order_by('id','desc')->get_all();

            $this->current = 'portfolio';
            $this->load->view($this->header, ['current' => $this->current]);
    
            $this->load->view('portfolio', $data);
            $this->load->view($this->footer);
        }

        public function services()
        {
            $this->current = 'services';
            $this->load->view($this->header, ['current' => $this->current]);
            
            $this->load->view('services');
            $this->load->view($this->footer);
        }

        public function teams()
        {
            $this->current = 'teams';
            $this->load->view($this->header, ['current' => $this->current]);
            
            $this->load->view('teams');
            $this->load->view($this->footer);
        }
}